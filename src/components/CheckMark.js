import React from 'react';
import styled, { keyframes } from 'react-emotion';
import PropTypes from 'prop-types';
import { colors } from '../constants';

const cubicBezierCurve = 'cubic-bezier(0.650, 0.000, 0.450, 1.000)';

const animationStroke = keyframes`
  100% {
    stroke-dashoffset: 0;
  }
`;

const animationScale = keyframes`
  0%, 100% {
    transform: none;
  }
  50% {
    transform: scale3d(1.1, 1.1, 1);
  }
`;

const animationFill = keyframes`
  100% {
    box-shadow: inset 0px 0px 0px 30px ${colors.success};
  }
`;

const SVG = styled.svg`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  display: block;
  stroke-width: 2;
  stroke: #fff;
  stroke-miterlimit: 10;
  box-shadow: inset 0px 0px 0px ${colors.success};
  animation: ${animationFill} .4s ease-in-out .4s forwards, ${animationScale} .3s ease-in-out .9s both;
`;

const Circle = styled.circle`
  stroke-dasharray: 166;
  stroke-dashoffset: 166;
  stroke-width: 50;
  stroke-miterlimit: 10;
  stroke: ${colors.success};
  fill: none;
  animation: ${animationStroke} .6s ${cubicBezierCurve} forwards;
`;

const Path = styled.path`
  transform-origin: 50% 50%;
  stroke-dasharray: 48;
  stroke-dashoffset: 48;
  animation: ${animationStroke} .3s ${cubicBezierCurve} .8s forwards;
`;

const propTypes = {
  isVisible: PropTypes.bool.isRequired,
};

const CheckMark = ({ isVisible }) => (
  <span>
    {isVisible &&
      <SVG className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
        <Circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
        <Path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
      </SVG>
    }
  </span>
);

CheckMark.propTypes = propTypes;

export default CheckMark;
