import React, { Component } from 'react';
import { render } from 'react-dom';
import smoothScroll from 'smoothscroll';
import NetworkTest from './components/NetworkTest';
import BrowserTest from './components/BrowserTest';
import Results from './components/Results';
import Footer from './components/Footer';


class App extends Component {
  constructor() {
    super();

    this.state = {
      testStatus: {
        network: 'ACTIVE',
        browser: 'INACTIVE',
      },
      results: {
        network: {
          upload: '0.00',
          download: '0.00',
          ping: '0.00',
        },
        browser: {
          total: 0,
          specs: {},
        },
        clientInfo: {
          ip: null,
        },
      },
    };

    this.networkTestContainer = null;
    this.browserTestContainer = null;
    this.resultsContainer = null;
    this.mailtoAddress = '';
  }

  onNetworkTestComplete = ({
    uploadSpeed,
    downloadSpeed,
    pingSpeed,
    clientInfo,
  }) => {
    this.setState({
      testStatus: {
        network: 'COMPLETED',
        browser: 'ACTIVE',
      },
      results: {
        ...this.state.results,
        network: {
          upload: uploadSpeed,
          download: downloadSpeed,
          ping: pingSpeed,
        },
        clientInfo,
      },
    });
    setTimeout(() => {
      smoothScroll(this.browserTestContainer.containerElement, 500);
    }, 1500);
  };

  componentDidMount = () => {
    fetch('/mailtoAddress').then(res => res.json()).then(({ email }) => {
      this.mailtoAddress = email;
    });
  }

  onBrowserTestComplete = ({ total, specs }) => {
    this.setState({
      testStatus: {
        network: 'COMPLETED',
        browser: 'COMPLETED',
      },
      results: {
        ...this.state.results,
        browser: {
          total,
          specs,
        },
      },
    });

    this.sendResultsToServer();

    setTimeout(() => {
      smoothScroll(this.resultsContainer.containerElement, 500);
    }, 1500);
  };

  sendResultsToServer() {
    fetch('/results', {
      method: 'POST',
      body: JSON.stringify(this.state.results),
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    });
  }

  render() {
    return (
      <div>
        <NetworkTest
          ref={(ref) => { this.networkTestContainer = ref; }}
          status={this.state.testStatus.network}
          onComplete={this.onNetworkTestComplete}
        />
        <BrowserTest
          ref={(ref) => { this.browserTestContainer = ref; }}
          status={this.state.testStatus.browser}
          onComplete={this.onBrowserTestComplete}
        />
        <Results
          data={this.state.results}
          ref={(ref) => { this.resultsContainer = ref; }}
          isRevealed={this.state.testStatus.browser === 'COMPLETED'}
          mailtoAddress={this.mailtoAddress}
        />
        <Footer isRevealed={this.state.testStatus.browser === 'COMPLETED'} />
      </div>
    );
  }
}

render(<App />, document.getElementById('app-root'));
