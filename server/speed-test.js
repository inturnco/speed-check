const randomBytes = require('random-bytes');
const ipLocation = require('iplocation');

module.exports = () => {
  const empty = (req, res) => {
    res.send('beep boop');
  };

  const garbage = (req, res) => {
    res.set({
      'Access-Control-Allow-Origin': '*',
      'Content-Description': 'File Transfer',
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': 'attachment; filename=random.dat',
      'Content-Transfer-Encoding': 'binary',
      // Never cache me
      'Cache-Control': 'no-store, no-cache, must-revalidate, max-age=0 post-check=0, pre-check=0, false',
      Pragma: 'no-cache',
    });

    const garbageData = randomBytes.sync(1048576 * 16);
    res.send(garbageData);
  };

  const getIp = (req, res) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    ipLocation(ip, ['https://ipapi.co/*/json/']).then(data => res.send(data));
  };

  return {
    empty,
    garbage,
    getIp,
  };
};
