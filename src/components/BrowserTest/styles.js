import styled, { css } from 'react-emotion';
import { colors } from '../../constants';

export const Container = styled.div`
  background: #f2f6f9;
  padding-bottom: 100px;
  position: relative;
`;

export const SquareWrapper = styled.div`
  margin: 5px;
  display: inline-block;
`;

export const IFrameWrapper = styled.div`
  width: 600px;
  height: 340px;
  margin-top: 5px;
  background: #111c25;
  font-family: Courier New, Courier, monospace;
  color: ${colors.success};
  overflow: auto;

  pre {
    padding: 20px;
  }
`;

export const Specs = styled.pre`
  width: 100%;
  color: #C8CAD5;
  text-shadow: 0 1px white;
`;
