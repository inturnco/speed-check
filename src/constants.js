export const colors = {
  secondary: '#8864a2',
  positive: 'rgba(15, 185, 241, 1)',
  negative: 'rgba(230, 33, 75, 1)',
  defaultText: '#232323',
  warning: '#FBCD77',
  success: '#5ead5e',
};

export const averageBrowserTestTime = 2500;
export const averageDownloadSpeed = 40.71;
export const averageUploadSpeed = 20.33;
