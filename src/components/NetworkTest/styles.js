import styled, { css } from 'react-emotion';

export const Container = styled.div`
  background: linear-gradient(#111c25, #06121b);
  position: relative;
`;

export const LoadingContainer = styled.div`
  position: absolute;
  transition: opacity 0.5s;
  left: 50%;
  transform: translateX(-50%);
  opacity: 0;

  ${({ isLoading }) => isLoading && css`
    opacity: 1;
  `}
`;

export const Results = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  padding: 70px 0;
`;

export const Stats = styled.pre`
  width: 100%;
  text-align: right;
  color: #47536f;
  margin: -18px 0 0;
`;
