import React, { Component } from 'react';
import Chart from 'chart.js';
import styled, { css } from 'react-emotion';

const ChartContainer = styled.div`
  position: relative;
  height: 300px;
  width: 100%;
`;

const networkSpeedChartStyles = css`
  height: 300px!important;
`;

const dataDefaults = {
  showLine: true,
  pointRadius: 0,
  borderWidth: 2,
};

const createChart = ctx => new Chart(ctx, {
  type: 'scatter',
  data: {
    datasets: [Object.assign({}, dataDefaults, {
      data: [
        { x: 0, y: 0 },
      ],
      label: 'download',
      borderColor: 'rgba(230, 33, 75, 1)',
      backgroundColor: 'rgba(230, 33, 75, 0.03)',
      xAxisID: 'download-x-axis',
    }),
    Object.assign({}, dataDefaults, {
      data: [
        { x: 0, y: 0 },
      ],
      label: 'upload',
      borderColor: 'rgba(15, 185, 241, 1)',
      backgroundColor: 'rgba(15, 185, 241, 0.02)',
      xAxisID: 'upload-x-axis',
    })],
  },
  options: {
    animation: {
      easing: 'linear',
    },
    legend: {
      display: false,
    },
    scales: {
      display: false,
      yAxes: [{
        ticks: {
          beginAtZero: true,
          display: false,
        },
        gridLines: {
          display: false,
        },
      }],
      xAxes: [{
        display: false,
        id: 'download-x-axis',
        ticks: {
          beginAtZero: true,
          display: false,
        },
        gridLines: {
          display: false,
        },
        type: 'linear',
      },
      {
        id: 'upload-x-axis',
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
        },
        type: 'linear',
      }],
    },
  },
});

export default class $Chart extends Component {
  constructor() {
    super();
    this.chartCanvas = null;
    this.chartInstance = null;
  }
  componentDidMount() {
    this.chartInstance = createChart(this.chartCanvas);
  }

  render() {
    return (
      <ChartContainer>
        <canvas
          ref={(ref) => { this.chartCanvas = ref; }}
          className={networkSpeedChartStyles}
        />
      </ChartContainer>
    );
  }
}
