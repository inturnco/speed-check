export default ({ network, browser, clientInfo }) => encodeURIComponent(`
  =============================
  Results
  =============================

  User:
  -----------------------------
  ip: ${clientInfo.ip}
  location: ${clientInfo.region_code} │ ${clientInfo.country} | ${clientInfo.postal}
  isp: ${clientInfo.org}

  Network:
  -----------------------------
  Download: ${network.download}Mbps
  Upload: ${network.upload}Mbps
  Ping: ${network.ping}ms

  Browser:
  -----------------------------
  Kraken Benchmark: ${browser.total}ms

  Hardware:
  -----------------------------
  Browser Renderer: ${browser.specs.browser.vendor} | ${browser.specs.browser.renderer}
  GPU: ${browser.specs.gpu.vendor} | ${browser.specs.gpu.renderer}
  Hardware Concurrency: ${browser.specs.threads}
`);
