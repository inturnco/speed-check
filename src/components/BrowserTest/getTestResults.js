/*
 * Copyright (C) 2007 Apple Inc.  All rights reserved.
 * Copyright (C) 2010 Mozilla Foundation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


import { categoryNames, testNames } from './config';

const tDistribution = [NaN, NaN, 12.71, 4.30, 3.18, 2.78, 2.57, 2.45, 2.36, 2.31, 2.26, 2.23, 2.20, 2.18, 2.16, 2.14, 2.13, 2.12, 2.11, 2.10, 2.09, 2.09, 2.08, 2.07, 2.07, 2.06, 2.06, 2.06, 2.05, 2.05, 2.05, 2.04, 2.04, 2.04, 2.03, 2.03, 2.03, 2.03, 2.03, 2.02, 2.02, 2.02, 2.02, 2.02, 2.02, 2.02, 2.01, 2.01, 2.01, 2.01, 2.01, 2.01, 2.01, 2.01, 2.01, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.99, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.98, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.97, 1.96]; // eslint-disable-line
const tMax = tDistribution.length;
const tLimit = 1.96;

let total = 0;
const categoryTotals = {};
const testTotalsByCategory = {};

let mean = 0;
const categoryMeans = {};
const testMeansByCategory = {};

let stdDev = 0;
const categoryStdDevs = {};
const testStdDevsByCategory = {};

let stdErr = 0;
const categoryStdErrs = {};
const testStdErrsByCategory = {};

export const getMeanTime = () => mean;

export default (output) => {
  const count = Object.keys(output).length;

  let itemTotals = {};
  itemTotals.length = count;

  function initialize() {
    itemTotals = { total: [] };

    categoryNames.forEach((category) => {
      itemTotals[category] = [];
      categoryTotals[category] = 0;
      testTotalsByCategory[category] = {};
      categoryMeans[category] = 0;
      testMeansByCategory[category] = {};
      categoryStdDevs[category] = 0;
      testStdDevsByCategory[category] = {};
      categoryStdErrs[category] = 0;
      testStdErrsByCategory[category] = {};
    });

    testNames.forEach((test) => {
      itemTotals[test] = [];
      const category = test.replace(/-.*/, '');
      testTotalsByCategory[category][test] = 0;
      testMeansByCategory[category][test] = 0;
      testStdDevsByCategory[category][test] = 0;
      testStdErrsByCategory[category][test] = 0;
    });

    Object.keys(output).forEach((_, idx) => {
      itemTotals.total[idx] = 0;
      Object.keys(categoryTotals).forEach((category) => {
        itemTotals[category][idx] = 0;
        Object.keys(testTotalsByCategory[category]).forEach((test) => {
          itemTotals[test][idx] = 0;
        });
      });
    });
  }

  function computeItemTotals() {
    Object.keys(output).forEach((result, idx) => {
      Object.keys(output[result]).forEach((test) => {
        const time = output[result][test];
        const category = test.replace(/-.*/, '');
        itemTotals.total[idx] += time;
        itemTotals[category][idx] += time;
        itemTotals[test][idx] += time;
      });
    });
  }

  function computeTotals() {
    Object.keys(output).forEach((result) => {
      Object.keys(output[result]).forEach((test) => {
        const time = output[result][test];
        const category = test.replace(/-.*/, '');
        total += time;
        categoryTotals[category] += time;
        testTotalsByCategory[category][test] += time;
      });
    });
  }

  function computeMeans() {
    mean = total / count;
    Object.keys(categoryTotals).forEach((category) => {
      categoryMeans[category] = categoryTotals[category] / count;
      Object.keys(testTotalsByCategory[category]).forEach((test) => {
        testMeansByCategory[category][test] = testTotalsByCategory[category][test] / count;
      });
    });
  }

  function standardDeviation(_mean, items) {
    let deltaSquaredSum = 0;
    for (let i = 0; i < items.length; i += 1) {
      const delta = items[i] - _mean;
      deltaSquaredSum += delta * delta;
    }
    const variance = deltaSquaredSum / (items.length - 1);
    return Math.sqrt(variance);
  }

  function computeStdDevs() {
    stdDev = standardDeviation(mean, itemTotals.total);
    Object.keys(categoryStdDevs).forEach((category) => {
      categoryStdDevs[category] = standardDeviation(categoryMeans[category], itemTotals[category]);
    });
    Object.keys(categoryStdDevs).forEach((category) => {
      Object.keys(testStdDevsByCategory[category]).forEach((test) => {
        testStdDevsByCategory[category][test] =
          standardDeviation(testMeansByCategory[category][test], itemTotals[test]);
      });
    });
  }

  function computeStdErrors() {
    const sqrtCount = Math.sqrt(count);
    stdErr = stdDev / sqrtCount;

    Object.keys(categoryStdErrs).forEach((category) => {
      categoryStdErrs[category] = categoryStdDevs[category] / sqrtCount;
    });

    Object.keys(categoryStdDevs).forEach((category) => {
      Object.keys(testStdErrsByCategory[category]).forEach((test) => {
        testStdErrsByCategory[category][test] = testStdDevsByCategory[category][test] / sqrtCount;
      });
    });
  }

  function tDist(n) {
    if (n > tMax) { return tLimit; }
    return tDistribution[n];
  }


  function formatResult(meanWidth, _mean, _stdErr, n) {
    let meanString = _mean.toFixed(1).toString();
    while (meanString.length < meanWidth) {
      meanString = ` ${meanString}`;
    }

    if (n === 1) { return `${meanString}ms`; }

    return `${meanString}ms +/- ${(((tDist(n) * _stdErr) / _mean) * 100).toFixed(1)}%`;
  }

  function computeLabelWidth() {
    let width = 'Total'.length;
    Object.keys(categoryMeans).forEach((category) => {
      if (category.length + 2 > width) { width = category.length + 2; }
    });
    testNames.forEach((test) => {
      const shortName = test.replace(/^[^-]*-/, '');
      if (shortName.length + 4 > width) {
        width = shortName.length + 4;
      }
    });

    return width;
  }

  function computeMeanWidth() {
    let width = mean.toFixed(1).toString().length;

    Object.keys(categoryMeans).forEach((category) => {
      const candidate = categoryMeans[category].toFixed(2).toString().length;
      if (candidate > width) {
        width = candidate;
      }
      Object.keys(testMeansByCategory[category]).forEach((test) => {
        const candidate2 = testMeansByCategory[category][test].toFixed(2).toString().length;
        if (candidate2 > width) {
          width = candidate2;
        }
      });
    });

    return width;
  }


  function resultLine(labelWidth, indent, label, meanWidth, _mean, _stdErr) {
    let result = '';
    for (let i = 0; i < indent; i += 1) {
      result += ' ';
    }

    result += `${label}: `;

    for (let i = 0; i < (labelWidth - (label.length + indent)); i += 1) {
      result += ' ';
    }

    return result + formatResult(meanWidth, _mean, _stdErr, count);
  }

  function getFormattedOutput() {
    const results = [];

    const labelWidth = computeLabelWidth();
    const meanWidth = computeMeanWidth();

    results.push('===============================================');
    if (count === 1) {
      results.push('RESULTS');
    } else {
      results.push('RESULTS (means and 95% confidence intervals)');
    }
    results.push('-----------------------------------------------');
    results.push(resultLine(labelWidth, 0, 'Total', meanWidth, mean, stdErr));
    results.push('-----------------------------------------------');

    Object.keys(categoryMeans).forEach((category) => {
      results.push('');
      results.push(resultLine(
        labelWidth,
        2,
        category,
        meanWidth,
        categoryMeans[category],
        categoryStdErrs[category],
      ));

      Object.keys(testMeansByCategory[category]).forEach((test) => {
        const shortName = test.replace(/^[^-]*-/, '');
        results.push(resultLine(
          labelWidth,
          4,
          shortName,
          meanWidth,
          testMeansByCategory[category][test],
          testStdErrsByCategory[category][test],
        ));
      });
    });

    return results.join('\n');
  }

  initialize();
  computeItemTotals();
  computeTotals();
  computeMeans();
  computeStdDevs();
  computeStdErrors();
  return getFormattedOutput();
};
