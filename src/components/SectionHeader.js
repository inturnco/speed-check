import React from 'react';
import styled, { css } from 'react-emotion';
import PropTypes from 'prop-types';

const Heading = styled.h2`
  font-size: 40px;
  color: rgba(39, 45, 60, 0.25);
  font-weight: 800;
  letter-spacing: 0px;
  text-transform: uppercase;
  margin: 0px 0 70px 8px;
  position: relative;

  ${({ theme }) => theme && theme === 'dark' && css`
    color: rgba(71, 83, 111, 0.4);
  `}
`;

const propTypes = {
  children: PropTypes.node.isRequired,
  theme: PropTypes.string,
};

const defaultProps = {
  theme: 'default',
};

const SectionHeader = ({ theme, children }) =>
  <Heading theme={theme}>{children}</Heading>;

SectionHeader.propTypes = propTypes;
SectionHeader.defaultProps = defaultProps;

export default SectionHeader;
