const path = require('path');

module.exports = {
  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty',
  },
  entry: ['./src/index.js'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|jpeg|svg)$/,
        use: ['file-loader'],
      },
    ],
  },
};
