import styled, { css } from 'react-emotion';

const CompletedContainer = styled.div`
  position: absolute;
  transition: opacity 0.5s;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  z-index: 1;
  pointer-events: none;
  background: rgba(93, 173, 93, 0.1);

  ${({ isCompleted }) => isCompleted && css`
    opacity: 1;
  `}
`;

export default CompletedContainer;
