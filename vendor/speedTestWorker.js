/* eslint-disable max-len,no-console */

/*
  HTML5 Speedtest v4.5
  by Federico Dossena
  https://github.com/adolfintel/speedtest/
  GNU LGPLv3 License
*/

// data reported to main thread
let testStatus = -1; // -1=not started, 0=starting, 1=download test, 2=ping+jitter test, 3=upload test, 4=finished, 5=abort/error
let downloadSpeed = 0; // download speed in megabit/s with 2 decimal digits
let downloadProgress = 0; // progress of download test 0-1
let uploadProgress = 0; // progress of upload test 0-1
let uploadSpeed = 0; // upload speed in megabit/s with 2 decimal digits
let pingTime = 0; // ping in milliseconds with 2 decimal digits
let pingProgress = 0; // progress of ping+jitter test 0-1
let jitterStatus = ''; // jitter in milliseconds with 2 decimal digits
let clientInfo = {}; // client's IP address as reported by getIP.php


// test settings. can be overridden by sending specific values with the start command
const settings = {
  test_order: 'IP_D_U', // order in which tests will be performed as a string. D=Download, U=Upload, P=Ping+Jitter, I=IP, _=1 second delay
  time_ul: 15, // duration of upload test in seconds
  time_dl: 15, // duration of download test in seconds
  time_ulGraceTime: 1, // time to wait in seconds before actually measuring ul speed (wait for buffers to fill)
  time_dlGraceTime: 1.5, // time to wait in seconds before actually measuring dl speed (wait for TCP window to increase)
  count_ping: 10, // number of pings to perform in ping test
  url_dl: '/speedtest/garbage', // path to a large file or garbage.php, used for download test. must be relative to this js file
  url_ul: '/speedtest/empty', // path to an empty file, used for upload test. must be relative to this js file
  url_ping: '/speedtest/empty', // path to an empty file, used for ping test. must be relative to this js file
  url_getIp: '/speedtest/get-ip', // path to getIP.php relative to this js file, or a similar thing that outputs the client's ip
  xhr_dlMultistream: 10, // number of download streams to use (can be different if enable_quirks is active)
  xhr_ulMultistream: 3, // number of upload streams to use (can be different if enable_quirks is active)
  xhr_multistreamDelay: 300, // how much concurrent requests should be delayed
  xhr_ignoreErrors: 1, // 0=fail on errors, 1=attempt to restart a stream if it fails, 2=ignore all errors
  xhr_dlUseBlob: false, // if set to true, it reduces ram usage but uses the hard drive (useful with large garbagePhp_chunkSize and/or high xhr_dlMultistream)
  garbagePhp_chunkSize: 20, // size of chunks sent by garbage.php (can be different if enable_quirks is active)
  enable_quirks: true, // enable quirks for specific browsers. currently it overrides settings to optimize for specific browsers, unless they are already being overridden with the start command
  ping_allowPerformanceApi: true, // if enabled, the ping test will attempt to calculate the ping more precisely using the Performance API. Currently works perfectly in Chrome, badly in Edge, and not at all in Firefox. If Performance API is not supported or the result is obviously wrong, a fallback is provided.
  overheadCompensationFactor: 1.06, // can be changed to compensatie for transport overhead. (see doc.md for some other values)
  useMebibits: false, // if set to true, speed will be reported in mebibits/s instead of megabits/s
  telemetry_level: 0, // 0=disabled, 1=basic (results only), 2=full (results+log)
  url_telemetry: 'telemetry.php', // path to the script that adds telemetry data to the database
};

let xhr = null; // array of currently active xhr requests
let interval = null; // timer used in tests
let testPointer = 0; // pointer to the next test to run inside settings.test_order

/*
  this function is used on URLs passed in the settings to determine whether we need a ? or an & as a separator
*/
function urlSep(url) { return url.match(/\?/) ? '&' : '?'; }


// stops all XHR activity, aggressively
function clearRequests() {
  if (xhr) {
    for (let i = 0; i < xhr.length; i += 1) {
      try { xhr[i].onprogress = null; xhr[i].onload = null; xhr[i].onerror = null; } catch (e) { console.warn(e); }
      try { xhr[i].upload.onprogress = null; xhr[i].upload.onload = null; xhr[i].upload.onerror = null; } catch (e) { console.warn(e); }
      try { xhr[i].abort(); } catch (e) { console.warn(e); }
      try { delete (xhr[i]); } catch (e) { console.warn(e); }
    }
    xhr = null;
  }
}


/*
  listener for commands from main thread to this worker.
  commands:
  -status: returns the current status as a string of values spearated by a semicolon (;) in this order: testStatus;downloadSpeed;uploadSpeed;pingTime;clientInfo;jitterStatus;downloadProgress;uploadProgress;pingProgress
  -abort: aborts the current test
  -start: starts the test. optionally, settings can be passed as JSON.
    example: start {"time_ul":"10", "time_dl":"10", "count_ping":"50"}
*/
this.addEventListener('message', (e) => {
  const params = e.data.split(' ');
  if (params[0] === 'status') { // return status
    postMessage({
      testStatus, downloadSpeed, uploadSpeed, pingTime, clientInfo, jitterStatus, downloadProgress, uploadProgress, pingProgress,
    });
  }
  if (params[0] === 'start' && testStatus === -1) { // start new test
    testStatus = 0;
    try {
      // parse settings, if present
      let s = {};
      try {
        const ss = e.data.substring(5);
        if (ss) s = JSON.parse(ss);
      } catch (err) { console.warn('Error parsing custom settings JSON. Please check your syntax'); }

      // copy custom settings
      Object.keys(s).forEach((key) => {
        if (typeof settings[key] !== 'undefined') {
          settings[key] = s[key];
        } else {
          console.warn(`Unknown setting ignored: ${key}`);
        }
      });

      // quirks for specific browsers. apply only if not overridden. more may be added in future releases
      if (settings.enable_quirks || (typeof s.enable_quirks !== 'undefined' && s.enable_quirks)) {
        const ua = navigator.userAgent;
        if (/Firefox.(\d+\.\d+)/i.test(ua)) {
          if (typeof s.xhr_ulMultistream === 'undefined') {
            // ff more precise with 1 upload stream
            settings.xhr_ulMultistream = 1;
          }
        }
        if (/Edge.(\d+\.\d+)/i.test(ua)) {
          if (typeof s.xhr_dlMultistream === 'undefined') {
            // edge more precise with 3 download streams
            settings.xhr_dlMultistream = 3;
          }
        }
        if (/Chrome.(\d+)/i.test(ua) && (!!self.fetch)) {
          if (typeof s.xhr_dlMultistream === 'undefined') {
            // chrome more precise with 5 streams
            settings.xhr_dlMultistream = 5;
          }
        }
        if (/Edge.(\d+\.\d+)/i.test(ua)) {
          // Edge 15 introduced a bug that causes onprogress events to not get fired, we have to use the "small chunks" workaround that reduces accuracy
          settings.forceIE11Workaround = true;
        }
      }

      // telemetry_level has to be parsed and not just copied
      if (typeof s.telemetry_level !== 'undefined') {
        if (s.telemetry_level === 'basic') {
          return 1;
        } else if (s.telemetry_level === 'full') {
          return 2;
        }

        return 0;
      }

      // transform test_order to uppercase, just in case
      settings.test_order = settings.test_order.toUpperCase();
    } catch (err) { console.warn(`Possible error in custom test settings. Some settings may not be applied. Exception: ${err}`); }
    // run the tests
    testPointer = 0;
    let iRun = false;
    let dRun = false;
    let uRun = false;
    let pRun = false;
    const runNextTest = () => {
      if (testPointer >= settings.test_order.length) { testStatus = 4; return; }
      switch (settings.test_order.charAt(testPointer)) {
        case 'I': testPointer += 1; if (iRun) { runNextTest(); return; } iRun = true; getIp(runNextTest); break;
        case 'D': testPointer += 1; if (dRun) { runNextTest(); return; } dRun = true; testStatus = 1; dlTest(runNextTest); break;
        case 'U': testPointer += 1; if (uRun) { runNextTest(); return; } uRun = true; testStatus = 3; ulTest(runNextTest); break;
        case 'P': testPointer += 1; if (pRun) { runNextTest(); return; } pRun = true; testStatus = 2; pingTest(runNextTest); break;
        case '_': testPointer += 1; setTimeout(runNextTest, 1000); break;
        default: testPointer += 1;
      }
    };
    runNextTest();
  }
  if (params[0] === 'abort') { // abort command
    clearRequests(); // stop all xhr activity
    runNextTest = null;
    if (interval) clearInterval(interval); // clear timer if present
    testStatus = 5; downloadSpeed = ''; uploadSpeed = ''; pingTime = ''; jitterStatus = ''; // set test as aborted
  }
});


// gets client's IP using url_getIp, then calls the done function
let ipCalled = false; // used to prevent multiple accidental calls to getIp
function getIp(done) {
  if (ipCalled) return; ipCalled = true; // getIp already called?
  xhr = new XMLHttpRequest();
  xhr.onload = () => {
    clientInfo = JSON.parse(xhr.responseText);
    done();
  };
  xhr.onerror = () => {
    done();
  };
  xhr.open('GET', `${settings.url_getIp + urlSep(settings.url_getIp)}r=${Math.random()}`, true);
  xhr.send();
}
// download test, calls done function when it's over
let dlCalled = false; // used to prevent multiple accidental calls to dlTest
function dlTest(done) {
  if (dlCalled) return; dlCalled = true; // dlTest already called?
  let totLoaded = 0.0; // total number of loaded bytes
  let startT = new Date().getTime(); // timestamp when test was started
  let graceTimeDone = false; // set to true after the grace time is past
  let failed = false; // set to true if a stream fails
  const xhr = [];
  // function to create a download stream. streams are slightly delayed so that they will not end at the same time
  const testStream = (i, delay) => {
    setTimeout(() => {
      if (testStatus !== 1) return; // delayed stream ended up starting after the end of the download test
      let prevLoaded = 0; // number of bytes loaded last time onprogress was called
      const x = new XMLHttpRequest();
      xhr[i] = x;
      xhr[i].onprogress = (event) => {
        if (testStatus !== 1) { try { x.abort(); } catch (e) { console.warn(e); } } // just in case this XHR is still running after the download test
        // progress event, add number of new loaded bytes to totLoaded
        const loadDiff = event.loaded <= 0 ? 0 : (event.loaded - prevLoaded);
        if (isNaN(loadDiff) || !isFinite(loadDiff) || loadDiff < 0) return; // just in case
        totLoaded += loadDiff;
        prevLoaded = event.loaded;
      };
      xhr[i].onload = () => {
        // the large file has been loaded entirely, start again
        try { xhr[i].abort(); } catch (e) { console.warn(e); } // reset the stream data to empty ram
        testStream(i, 0);
      };
      xhr[i].onerror = () => {
        // error
        if (settings.xhr_ignoreErrors === 0) failed = true; // abort
        try { xhr[i].abort(); } catch (e) { console.warn(e); }
        delete (xhr[i]);
        if (settings.xhr_ignoreErrors === 1) testStream(i, 0); // restart stream
      };
      // send xhr
      try { if (settings.xhr_dlUseBlob) xhr[i].responseType = 'blob'; else xhr[i].responseType = 'arraybuffer'; } catch (e) { console.warn(e); }
      xhr[i].open('GET', `${settings.url_dl + urlSep(settings.url_dl)}r=${Math.random()}&ckSize=${settings.garbagePhp_chunkSize}`, true); // random string to prevent caching
      xhr[i].send();
    }, 1 + delay);
  };
  // open streams
  for (let i = 0; i < settings.xhr_dlMultistream; i += 1) {
    testStream(i, settings.xhr_multistreamDelay * i);
  }
  // every 200ms, update downloadSpeed
  interval = setInterval(() => {
    const t = new Date().getTime() - startT;
    if (graceTimeDone) downloadProgress = t / (settings.time_dl * 1000);
    if (t < 200) return;
    if (!graceTimeDone) {
      if (t > 1000 * settings.time_dlGraceTime) {
        if (totLoaded > 0) { // if the connection is so slow that we didn't get a single chunk yet, do not reset
          startT = new Date().getTime();
          totLoaded = 0.0;
        }
        graceTimeDone = true;
      }
    } else {
      const speed = totLoaded / (t / 1000.0);
      downloadSpeed = ((speed * 8 * settings.overheadCompensationFactor) / (settings.useMebibits ? 1048576 : 1000000)).toFixed(2); // speed is multiplied by 8 to go from bytes to bits, overhead compensation is applied, then everything is divided by 1048576 or 1000000 to go to megabits/mebibits
      if (((t / 1000.0) > settings.time_dl && downloadSpeed > 0) || failed) { // test is over, stop streams and timer
        if (failed || isNaN(downloadSpeed)) downloadSpeed = 'Fail';
        clearRequests();
        clearInterval(interval);
        downloadProgress = 1;
        done();
      }
    }
  }, 200);
}
// upload test, calls done function whent it's over
// garbage data for upload test
let r = new ArrayBuffer(1048576);
try { r = new Float32Array(r); for (let i = 0; i < r.length; i += 1)r[i] = Math.random(); } catch (e) { console.warn(e); }
let req = [];
let reqsmall = [];
for (let i = 0; i < 20; i += 1) req.push(r);
req = new Blob(req);
r = new ArrayBuffer(262144);
try { r = new Float32Array(r); for (let i = 0; i < r.length; i += 1)r[i] = Math.random(); } catch (e) { console.warn(e); }
reqsmall.push(r);
reqsmall = new Blob(reqsmall);

let ulCalled = false; // used to prevent multiple accidental calls to ulTest
function ulTest(done) {
  if (ulCalled) return; ulCalled = true; // ulTest already called?
  let totLoaded = 0.0; // total number of transmitted bytes
  let startT = new Date().getTime(); // timestamp when test was started
  let graceTimeDone = false; // set to true after the grace time is past
  let failed = false; // set to true if a stream fails
  const xhr = [];
  // function to create an upload stream. streams are slightly delayed so that they will not end at the same time
  const testStream = (i) => {
    setTimeout(() => {
      if (testStatus !== 3) return; // delayed stream ended up starting after the end of the upload test
      let prevLoaded = 0; // number of bytes transmitted last time onprogress was called
      const x = new XMLHttpRequest();
      xhr[i] = x;
      let ie11workaround;
      if (settings.forceIE11Workaround) ie11workaround = true; else {
        try {
          xhr[i].upload.onprogress;
          ie11workaround = false;
        } catch (e) {
          ie11workaround = true;
        }
      }
      if (ie11workaround) {
        // IE11 workarond: xhr.upload does not work properly, therefore we send a bunch of small 256k requests and use the onload event as progress. This is not precise, especially on fast connections
        xhr[i].onload = () => {
          totLoaded += reqsmall.size;
          testStream(i, 0);
        };
        xhr[i].onerror = () => {
          // error, abort
          if (settings.xhr_ignoreErrors === 0) failed = true; // abort
          try { xhr[i].abort(); } catch (e) { console.warn(e); }
          delete (xhr[i]);
          if (settings.xhr_ignoreErrors === 1) testStream(i, 0); // restart stream
        };
        xhr[i].open('POST', `${settings.url_ul + urlSep(settings.url_ul)}r=${Math.random()}`, true); // random string to prevent caching
        xhr[i].setRequestHeader('Content-Encoding', 'identity'); // disable compression (some browsers may refuse it, but data is incompressible anyway)
        xhr[i].send(reqsmall);
      } else {
        // REGULAR version, no workaround
        xhr[i].upload.onprogress = (event) => {
          if (testStatus !== 3) { try { x.abort(); } catch (e) { console.warn(e); } } // just in case this XHR is still running after the upload test
          // progress event, add number of new loaded bytes to totLoaded
          const loadDiff = event.loaded <= 0 ? 0 : (event.loaded - prevLoaded);
          if (isNaN(loadDiff) || !isFinite(loadDiff) || loadDiff < 0) return; // just in case
          totLoaded += loadDiff;
          prevLoaded = event.loaded;
        };
        xhr[i].upload.onload = () => {
          // this stream sent all the garbage data, start again
          testStream(i, 0);
        };
        xhr[i].upload.onerror = () => {
          if (settings.xhr_ignoreErrors === 0) failed = true; // abort
          try { xhr[i].abort(); } catch (e) { console.warn(e); }
          delete (xhr[i]);
          if (settings.xhr_ignoreErrors === 1) testStream(i, 0); // restart stream
        };
        // send xhr
        xhr[i].open('POST', `${settings.url_ul + urlSep(settings.url_ul)}r=${Math.random()}`, true); // random string to prevent caching
        xhr[i].setRequestHeader('Content-Encoding', 'identity'); // disable compression (some browsers may refuse it, but data is incompressible anyway)
        xhr[i].send(req);
      }
    }, 1);
  };
  // open streams
  for (let i = 0; i < settings.xhr_ulMultistream; i += 1) {
    testStream(i, settings.xhr_multistreamDelay * i);
  }
  // every 200ms, update uploadSpeed
  interval = setInterval(() => {
    const t = new Date().getTime() - startT;
    if (graceTimeDone) uploadProgress = t / (settings.time_ul * 1000);
    if (t < 200) return;
    if (!graceTimeDone) {
      if (t > 1000 * settings.time_ulGraceTime) {
        if (totLoaded > 0) { // if the connection is so slow that we didn't get a single chunk yet, do not reset
          startT = new Date().getTime();
          totLoaded = 0.0;
        }
        graceTimeDone = true;
      }
    } else {
      const speed = totLoaded / (t / 1000.0);
      uploadSpeed = ((speed * 8 * settings.overheadCompensationFactor) / (settings.useMebibits ? 1048576 : 1000000)).toFixed(2); // speed is multiplied by 8 to go from bytes to bits, overhead compensation is applied, then everything is divided by 1048576 or 1000000 to go to megabits/mebibits
      if (((t / 1000.0) > settings.time_ul && uploadSpeed > 0) || failed) { // test is over, stop streams and timer
        if (failed || isNaN(uploadSpeed)) uploadSpeed = 'Fail';
        clearRequests();
        clearInterval(interval);
        uploadProgress = 1;
        done();
      }
    }
  }, 200);
}
// ping+jitter test, function done is called when it's over
let ptCalled = false; // used to prevent multiple accidental calls to pingTest
function pingTest(done) {
  if (ptCalled) return; ptCalled = true; // pingTest already called?
  let prevT = null; // last time a pong was received
  let ping = 0.0; // current ping value
  let jitter = 0.0; // current jitter value
  let i = 0; // counter of pongs received
  let prevInstspd = 0; // last ping time, used for jitter calculation
  xhr = [];
  // ping function
  const doPing = () => {
    pingProgress = i / settings.count_ping;
    prevT = new Date().getTime();
    xhr[0] = new XMLHttpRequest();
    xhr[0].onload = () => {
      // pong
      if (i === 0) {
        prevT = new Date().getTime(); // first pong
      } else {
        let instspd = new Date().getTime() - prevT;
        if (settings.ping_allowPerformanceApi) {
          try {
            // try to get accurate performance timing using performance api
            let p = performance.getEntries();
            p = p[p.length - 1];
            let d = p.responseStart - p.requestStart; // best precision: chromium-based
            if (d <= 0) d = p.duration; // edge: not so good precision because it also considers the overhead and there is no way to avoid it
            if (d > 0 && d < instspd) instspd = d;
          } catch (e) {
            // if not possible, keep the estimate
            // firefox can't access performance api from worker: worst precision
          }
        }
        const instjitter = Math.abs(instspd - prevInstspd);
        if (i === 1) ping = instspd; /* first ping, can't tell jitter yet */ else {
          ping = ping * 0.9 + instspd * 0.1; // ping, weighted average
          jitter = instjitter > jitter ? (jitter * 0.2 + instjitter * 0.8) : (jitter * 0.9 + instjitter * 0.1); // update jitter, weighted average. spikes in ping values are given more weight.
        }
        prevInstspd = instspd;
      }
      pingTime = ping.toFixed(2);
      jitterStatus = jitter.toFixed(2);
      i += 1;
      if (i < settings.count_ping) doPing(); else { pingProgress = 1; done(); } // more pings to do?
    };
    xhr[0].onerror = () => {
      // a ping failed, cancel test
      if (settings.xhr_ignoreErrors === 0) { // abort
        pingTime = 'Fail';
        jitterStatus = 'Fail';
        clearRequests();
        done();
      }
      if (settings.xhr_ignoreErrors === 1) doPing(); // retry ping
      if (settings.xhr_ignoreErrors === 2) { // ignore failed ping
        i += 1;
        if (i < settings.count_ping) doPing(); else done(); // more pings to do?
      }
    };
    // sent xhr
    xhr[0].open('GET', `${settings.url_ping + urlSep(settings.url_ping)}r=${Math.random()}`, true); // random string to prevent caching
    xhr[0].send();
  };
  doPing(); // start first ping
}

