export const testNames = [
  'ai-astar',
  'audio-beat-detection',
  'audio-dft',
  'audio-fft',
  'audio-oscillator',
  'imaging-gaussian-blur',
  'imaging-darkroom',
  'imaging-desaturate',
  'json-parse-financial',
  'json-stringify-tinderbox',
  'stanford-crypto-aes',
  'stanford-crypto-ccm',
  'stanford-crypto-pbkdf2',
  'stanford-crypto-sha256-iterative',
];

export const categoryNames = [
  'ai',
  'audio',
  'imaging',
  'json',
  'stanford',
];

export const repeatCount = 10;
