import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Chart from './Chart';
import Row from '../../components/Row';
import ResultBox from '../ResultBox';
import Loading from '../Loading';
import SectionHeader from '../SectionHeader';
import CheckMark from '../CheckMark';
import CompletedContainer from '../CompletedContainer';
import { Container, LoadingContainer, Results, Stats } from './styles';


export default class NetworkTest extends Component {
  static propTypes = {
    status: PropTypes.oneOf(['INACTIVE', 'ACTIVE', 'COMPLETED']).isRequired,
    onComplete: PropTypes.func.isRequired,
  }

  constructor() {
    super();

    this.state = {
      networkSpeed: {
        upload: 0.00,
        download: 0.00,
        ping: 0.00,
      },
      isLoading: true,
      clientInfo: {
        ip: null,
      },
    };

    this.chartComponent = null;
    this.speedTestInterval = null;

    this.progressIntervals = {
      download: null,
      upload: null,
    };

    this.progress = {
      download: 0,
      upload: 0,
    };
  }

  componentDidMount() {
    const worker = new Worker('/speedtest/worker.js');
    this.speedTestInterval = setInterval(() => { worker.postMessage('status'); }, 50);
    worker.onmessage = this.handleWorkerMessage;
    worker.postMessage('start');
  }

  setUpdateInterval = name => setInterval(() => this.updateChart(name), 100);

  clearUpdateInterval = name => clearInterval(this.progressIntervals[name]);

  endSpeedTest = () => {
    clearInterval(this.speedTestInterval);
    this.props.onComplete({
      downloadSpeed: this.state.networkSpeed.download,
      uploadSpeed: this.state.networkSpeed.upload,
      pingSpeed: this.state.networkSpeed.ping,
      clientInfo: this.state.clientInfo,
    });
  }

  handleWorkerMessage = (event) => {
    const {
      testStatus,
      pingTime,
      downloadSpeed,
      downloadProgress,
      uploadSpeed,
      uploadProgress,
      clientInfo,
    } = event.data;

    if (!this.state.clientInfo.ip) {
      this.setState({
        clientInfo,
      });
    }

    if (testStatus === 1) {
      this.handleSpeedTestProgress('download')(downloadProgress * 100);
      this.handleSpeedTestData('download')(downloadSpeed);
    } else if (testStatus === 2) {
      this.setState({
        networkSpeed: {
          ...this.state.networkSpeed,
          ping: pingTime,
        },
      });
    } else if (testStatus === 3) {
      this.handleSpeedTestProgress('upload')(uploadProgress * 100);
      this.handleSpeedTestData('upload')(uploadSpeed);
    } else if (testStatus === 4) {
      this.endSpeedTest();
    }
  }

  updateChart(name) {
    const chart = this.chartComponent.chartInstance;

    const dataSet = chart.data.datasets.filter(_dataSet => _dataSet.label === name)[0];

    if (!dataSet) return;

    dataSet.data.push({ x: this.progress[name] += 1, y: this.state.networkSpeed[name] });
    chart.update();
  }


  handleSpeedTestData = name => (data) => {
    this.setState({
      networkSpeed: {
        ...this.state.networkSpeed,
        [name]: Number(data).toFixed(2),
      },
    });
  };

  handleSpeedTestProgress = name => (data) => {
    if (data > 0 && !this.progressIntervals[name]) {
      if (this.state.isLoading) {
        this.setState({ isLoading: false });
      }
      this.progressIntervals[name] = this.setUpdateInterval(name);
    }

    if (data >= 99 && this.progressIntervals[name]) {
      this.progressIntervals[name] = this.clearUpdateInterval(name);
    }
  };

  render() {
    return (
      <Container>
        <SectionHeader theme="dark">Network Speed</SectionHeader>
        <CompletedContainer isCompleted={this.props.status === 'COMPLETED'}>
          <CheckMark isVisible={this.props.status === 'COMPLETED'} />
        </CompletedContainer>
        <Row>
          <Results>
            <ResultBox
              value={this.state.networkSpeed.download}
              theme="negative"
              label="Download Speed"
              unit="Mbps"
              size="large"
            />
            <ResultBox
              value={this.state.networkSpeed.upload}
              theme="positive"
              label="Upload Speed"
              unit="Mbps"
              size="large"
            />
          </Results>
        </Row>
        <Row>
          <LoadingContainer isLoading={this.state.isLoading}>
            <Loading />
          </LoadingContainer>
          <Chart ref={(ref) => { this.chartComponent = ref; }} />
        </Row>
        {this.state.clientInfo.ip &&
          <Row>
            <Stats>
              Ping: {this.state.networkSpeed.ping}ms │{' '}
              {this.state.clientInfo.ip} │{' '}
              {this.state.clientInfo.region_code} │{' '}
              {this.state.clientInfo.country} │{' '}
              {this.state.clientInfo.org}
            </Stats>
          </Row>
        }
      </Container>
    );
  }
}
