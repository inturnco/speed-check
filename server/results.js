const Logger = require('nedb-logger');
const path = require('path');

const logger = new Logger({ filename: path.join(__dirname, '..', 'results.db') });

module.exports = (req, res) => {
  const data = Object.assign({}, req.body, { timestamp: new Date().getTime() });

  logger.insert(data);
  res.send('ok');
};
