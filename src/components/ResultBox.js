import React from 'react';
import styled, { css } from 'react-emotion';
import PropTypes from 'prop-types';
import { colors } from '../constants';

const Unit = styled.span`
  color: #48546f;
  font-size: 40px;
  font-weight: 100;
`;

const Value = styled.span``;

const Label = styled.h4`
  text-transform: uppercase;
  letter-spacing: 4px;
  color: #48546f;
  font-weight: lighter;
  font-size: 18px;
`;

const Box = styled.div`
  text-align: center;
  display: inline-flex;
  flex-direction: column;

  ${({ theme }) => theme && css`
    ${Value} {
      color: ${colors[theme]};
    }
  `}

  ${({ size }) => {
    if (size === 'large') {
      return css` ${Value} { font-size: 90px; }`;
    } else if (size === 'medium') {
      return css`
        h1 { margin-bottom: 0; }
        ${Value} { font-size: 60px;}
        ${Unit} { font-size: 28px; }
        ${Label} { font-size: 14px; font-weight: bold;}
      `;
    }
    return '';
  }}
  `;

const propTypes = {
  theme: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  label: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
};

const defaultProps = {
  size: 'medium',
};

const ResultBox = props => (
  <Box size={props.size} theme={props.theme}>
    <h1>
      <Value>{props.value}</Value>
      <Unit>{props.unit}</Unit>
    </h1>
    <Label>{props.label}</Label>
  </Box>
);

ResultBox.propTypes = propTypes;
ResultBox.defaultProps = defaultProps;

export default ResultBox;
