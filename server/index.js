/* eslint-disable no-console */

const app = require('express')();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const config = require('config');
const setupRoutes = require('./routes');

app.use(bodyParser.json())

setupRoutes(app);


http.listen(config.server.port, () => {
  console.log(`listening on port ${config.server.port}`);
});

