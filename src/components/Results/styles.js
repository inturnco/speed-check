import styled, { css } from 'react-emotion';
import { colors } from '../../constants';

export const Container = styled.div`
  background: white;
  position: relative;
  height: 0px;
  transition: height 0.2s;
  overflow: hidden;
  background-image: url('/geometry2.png');

  ${({ isRevealed }) => isRevealed && css`
    height: 650px;
  `}
`;

export const ResultsContainer = styled.div`
  margin: 50px auto 0;
  text-align: center;
  width: 100%;
  flex-direction: 'column';
  justify-content: space-around;
`;

export const BenchmarkDiff = styled.div`
  color: #adb8c1;
  font-weight: 100;
  font-size: 14px;

  ${({ theme }) => {
    if (colors[theme]) {
      return css`
        strong, em { color: ${colors[theme]}; }
      `;
    }
    return '';
  }}
  `;

export const RerunLink = styled.a`
  color: #b9c2cb;
  margin-top: 20px;
  display: inline-block;
  font-size: 18px;
  `;

export const ResultBoxWrapper = styled.div`
  padding: 60px 30px;
  background: rgba(255, 255, 255, 0.92);
  border-radius: 10px;
  `;

export const Button = styled.a`
  text-decoration: none;
  padding: 15px 50px;
  color: white;
  border-radius: 3px;
  display: block;
  font-size: 18px;
  font-weight: bold;
  background: ${colors.secondary};
  text-transform: uppercase;
  font-size: 16px;
  text-align: center;
  letter-spacing: 3px;
  border-bottom: 2px solid #65588e;
  box-shadow: 0 1px 2px #00000040;
  max-width: 300px;

  &:hover {
    background: #956fb1;
  }
  `;
