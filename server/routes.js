const path = require('path');
const express = require('express');
const setupSpeedTest = require('./speed-test');
const resultHandler = require('./results');
const config = require('config');

const speedTest = setupSpeedTest();

module.exports = (app) => {
  app.get(['/', '/speed-check'], (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'index.html'));
  });

  app.get('/healthcheck', (req, res) => {
    res.status(200).send('⚡ Ready to go!');
  });

  app.get('/bundle.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'dist/bundle.js'));
  });

  app.get('/kraken.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'src/components/BrowserTest/kraken-iframe.css'));
  });

  app.get('/mailtoAddress', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send({ email: config.mailtoAddress });
  });

  app.post('/results', resultHandler);

  app.get('/speedtest/garbage', speedTest.garbage);
  app.get('/speedtest/empty', speedTest.empty);
  app.post('/speedtest/empty', speedTest.empty);
  app.get('/speedtest/get-ip', speedTest.getIp);

  app.get('/speedtest/worker.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'vendor/speedTestWorker.js'));
  });

  app.use(express.static('public'));
};
