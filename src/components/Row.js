import React from 'react';
import styled, { css } from 'react-emotion';
import PropTypes from 'prop-types';

const Row = styled.div`
  max-width: 960px;
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  position: relative;

  ${({ centerContent }) => centerContent && css`
    justify-content: center;
  `}
`;

const propTypes = {
  children: PropTypes.node.isRequired,
  centerContent: PropTypes.bool,
};

const defaultProps = {
  centerContent: false,
};

const $Row = ({ children, centerContent }) => (
  <Row centerContent={centerContent}>{children}</Row>
);
$Row.propTypes = propTypes;
$Row.defaultProps = defaultProps;

export default $Row;

