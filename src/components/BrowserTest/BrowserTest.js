import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Row from '../../components/Row';
import testContents from './krakenTests';
import { testNames, repeatCount } from './config';
import Square from './Square';
import SectionHeader from '../SectionHeader';
import getFormattedTestResults, { getMeanTime } from './getTestResults';
import CheckMark from '../CheckMark';
import CompletedContainer from '../CompletedContainer';
import {
  Container,
  IFrameWrapper,
  Specs,
  SquareWrapper,
} from './styles';

const getUnmaskedInfo = (gl) => {
  const unMaskedInfo = {
    renderer: '',
    vendor: '',
  };

  const dbgRenderInfo = gl.getExtension('WEBGL_debug_renderer_info');
  if (dbgRenderInfo) {
    unMaskedInfo.renderer = gl.getParameter(dbgRenderInfo.UNMASKED_RENDERER_WEBGL);
    unMaskedInfo.vendor = gl.getParameter(dbgRenderInfo.UNMASKED_VENDOR_WEBGL);
  }

  return unMaskedInfo;
};

export default class BrowserTest extends Component {
  static propTypes = {
    status: PropTypes.oneOf(['INACTIVE', 'ACTIVE', 'COMPLETED']).isRequired,
    onComplete: PropTypes.func.isRequired,
  }

  constructor() {
    super();

    this.testIndex = -1;
    this.currentRepeat = -1;
    this.repeatCount = repeatCount;

    this.frameParent = null;
    this.containerElement = null;
    this.glCanvasElement = null;
    this.output = {};

    const tests = testNames.reduce((acc, testName) => {
      const repetitions = [...Array(this.repeatCount).keys()].reduce((_tests, _, idx) => {
        const testReps = _tests;
        const test = {
          status: 'INACTIVE',
          id: `${testName}-${idx}`,
        };

        testReps[idx] = test;
        return testReps;
      }, {});

      acc[testName] = repetitions;
      return acc;
    }, {});

    this.state = {
      tests,
      specs: {
        browser: {
          renderer: null,
          vendor: null,
        },
        gpu: {
          vendor: null,
          renderer: null,
        },
        threads: null,
      },
    };
  }


  componentDidMount() {
    window.addEventListener('message', (message) => {
      if (message.data.testCompleted) {
        this.recordResult(message.data.time);
      }
    });
    this.getSpecs();
    this.frameParent.innerHTML = '<pre style="font-size:16px">Test will begin momentarily...</pre>';
  }

  componentDidUpdate(newProps) {
    if (this.props.status === newProps.status) return;

    if (this.props.status === 'ACTIVE') {
      setTimeout(this.runTest, 3000);
    }
  }

  getSpecs = () => {
    const gl = this.glCanvasElement.getContext('experimental-webgl');
    this.setState({
      specs: {
        browser: {
          renderer: gl.getParameter(gl.RENDERER),
          vendor: gl.getParameter(gl.VENDOR),
        },
        gpu: {
          vendor: getUnmaskedInfo(gl).vendor,
          renderer: getUnmaskedInfo(gl).renderer,
        },
        threads: window.navigator.hardwareConcurrency,
      },
    });
  }

  setSquareStatus = (status) => {
    if (this.currentRepeat === -1) return;
    const currentTestName = testNames[this.testIndex];

    if (!this.state.tests[currentTestName]) return;

    this.setState({
      tests: {
        ...this.state.tests,
        [currentTestName]: {
          ...this.state.tests[currentTestName],
          [this.currentRepeat]: {
            ...this.state.tests[currentTestName][this.currentRepeat],
            status,
          },
        },
      },
    });
  }

  finish = () => {
    this.frameParent.innerHTML = `<pre>${getFormattedTestResults(this.output)}</pre>`;
    this.props.onComplete({ total: getMeanTime(), specs: this.state.specs });
  }

  runTest = () => {
    this.testIndex += 1;
    this.setSquareStatus('ACTIVE');

    const testFrame = document.createElement('iframe');
    if (!this.frameParent) return;

    this.frameParent.innerHTML = '';
    this.frameParent.appendChild(testFrame);


    if (this.testIndex < testNames.length) {
      testFrame.contentDocument.open();
      testFrame.contentDocument.write(testContents[this.testIndex]);
      testFrame.contentDocument.close();
    } else if (++this.currentRepeat < this.repeatCount) { // eslint-disable-line
      this.testIndex = 0;
      testFrame.contentDocument.open();
      testFrame.contentDocument.write(testContents[this.testIndex]);
      testFrame.contentDocument.close();
    } else {
      this.finish();
    }
  }

  recordResult = (time) => {
    // negative repeats are warmups
    if (this.currentRepeat >= 0) {
      this.output[this.currentRepeat] = this.output[this.currentRepeat] || {};
      this.output[this.currentRepeat][testNames[this.testIndex]] = time;
      this.setSquareStatus('COMPLETED');
    }
    this.runNextTest();
  }

  runNextTest = () => {
    window.setTimeout(this.runTest, 16);
  }

  render() {
    const { tests } = this.state;

    return (
      <Container innerRef={(ref) => { this.containerElement = ref; }}>
        <SectionHeader>Browser Performance</SectionHeader>
        <CompletedContainer isCompleted={this.props.status === 'COMPLETED'}>
          <CheckMark isVisible={this.props.status === 'COMPLETED'} />
        </CompletedContainer>
        <Row>
          <IFrameWrapper innerRef={(ref) => { this.frameParent = ref; }} />
          <canvas ref={(ref) => { this.glCanvasElement = ref; }} height="0" width="0" />
          <div className="squares">
            {Object.keys(tests).map(testName => (
              <Row key={testName}>
                {Object.keys(tests[testName]).map(testRepetition => (
                  <SquareWrapper key={tests[testName][testRepetition].id}>
                    <Square status={tests[testName][testRepetition].status} />
                  </SquareWrapper>
                ))
              }
              </Row>
            ))}
          </div>
        </Row>
        <Row>
          <Specs>
            <div>
              ⬓ <strong>Browser</strong>:
              {' '}{this.state.specs.browser.vendor} │
              {' '}<em>{this.state.specs.browser.renderer}</em>
            </div>

            <div style={{ marginLeft: '-2px' }}>
              ⬠ <strong style={{ marginLeft: '-1px' }}>Hardware</strong>:
              {' '}{this.state.specs.gpu.vendor} │
              {' '}<em>{this.state.specs.gpu.renderer}</em>
            </div>

            {this.state.specs.threads &&
              <div>
                ⦿ <strong>Cores</strong>: {this.state.specs.threads}
              </div>
            }
          </Specs>
        </Row>
      </Container>
    );
  }
}
