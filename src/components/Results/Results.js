import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SectionHeader from '../SectionHeader';
import { Container, ResultsContainer, Button, BenchmarkDiff, RerunLink, ResultBoxWrapper } from './styles';
import { averageBrowserTestTime, averageDownloadSpeed, averageUploadSpeed } from '../../constants';
import emailBodyTemplate from './emailBodyTemplate';
import Row from '../Row';
import ResultBox from '../ResultBox';

const getPercentageDiff = (_value, _average) => {
  let isBetterThanAverage = true;
  let value = _value;
  let average = _average;
  if (_value < _average) {
    isBetterThanAverage = false;
    value = _average;
    average = _value;
  }

  return {
    percentage: (((value / average) * 100).toFixed(0)) - 100,
    isBetterThanAverage,
  };
};

class Results extends Component {
  static propTypes = {
    isRevealed: PropTypes.bool.isRequired,
    data: PropTypes.shape({
      browser: PropTypes.shape({
        total: PropTypes.number.isRequired,
        specs: PropTypes.object,
      }).isRequired,
      network: PropTypes.shape({
        download: PropTypes.string.isRequired,
        upload: PropTypes.string.isRequired,
        ping: PropTypes.string.isRequired,
      }).isRequired,
      clientInfo: PropTypes.object.isRequired,
    }).isRequired,
    mailtoAddress: PropTypes.string.isRequired,
  }

  constructor() {
    super();
    this.containerElement = null;
  }

  getFormattedBrowserBenchmark() {
    return Number((this.props.data.browser.total).toFixed(2));
  }

  getMailToLink() {
    const { browser, network, clientInfo } = this.props.data;
    const subject = 'INTURN Performance Test Results';
    const body = emailBodyTemplate({ browser, network, clientInfo });

    return `mailto:${this.props.mailtoAddress}?subject=${subject}&body=${body}`;
  }

  render() {
    const { network } = this.props.data;
    const browserTotal = this.getFormattedBrowserBenchmark();

    const {
      percentage: browserDiff,
      isBetterThanAverage: isBrowserFasterThanAverage,
    } = getPercentageDiff(averageBrowserTestTime, Number(browserTotal));

    const {
      percentage: downloadDiff,
      isBetterThanAverage: isDownloadFasterThanAverage,
    } = getPercentageDiff(Number(network.download), averageDownloadSpeed);

    const {
      percentage: uploadDiff,
      isBetterThanAverage: isUploadFasterThanAverage,
    } = getPercentageDiff(Number(network.upload), averageUploadSpeed);

    return (
      <Container
        innerRef={(ref) => { this.containerElement = ref; }}
        isRevealed={this.props.isRevealed}
      >
        <SectionHeader>Results</SectionHeader>
        {this.props.isRevealed &&
          <Row>
            <ResultsContainer>
              <Row>
                <ResultBoxWrapper>
                  <ResultBox
                    value={network.download}
                    theme="negative"
                    label="Download Speed"
                    unit="Mbps"
                    size="medium"
                  />
                  <BenchmarkDiff theme={isDownloadFasterThanAverage ? 'success' : 'negative'}>
                    <strong>{Math.abs(downloadDiff)}%</strong>
                    <em> {isDownloadFasterThanAverage ? 'faster' : 'slower'}</em> than the global average<sup>*</sup>
                  </BenchmarkDiff>
                </ResultBoxWrapper>
                <ResultBoxWrapper>
                  <ResultBox
                    value={network.upload}
                    theme="positive"
                    label="Upload Speed"
                    unit="Mbps"
                    size="medium"
                  />
                  <BenchmarkDiff theme={isUploadFasterThanAverage ? 'success' : 'negative'}>
                    <strong>{Math.abs(uploadDiff)}%</strong>
                    <em> {isUploadFasterThanAverage ? 'faster' : 'slower'}</em> than the global average<sup>*</sup>
                  </BenchmarkDiff>
                </ResultBoxWrapper>
                <ResultBoxWrapper>
                  <ResultBox
                    value={browserTotal}
                    theme="default"
                    label="Browser Benchmark"
                    unit="ms"
                    size="medium"
                  />
                  <BenchmarkDiff theme={isBrowserFasterThanAverage ? 'success' : 'negative'}>
                    <strong>{Math.abs(browserDiff)}%</strong>
                    <em> {isBrowserFasterThanAverage ? 'faster' : 'slower'}</em> than average
                  </BenchmarkDiff>
                </ResultBoxWrapper>
              </Row>

              <div style={{ marginTop: '50px' }}>
                <Row centerContent>
                  <Button href={this.getMailToLink()}>Email us the results</Button>
                </Row>
                <Row centerContent>
                  <RerunLink href="/">Rerun the test</RerunLink>
                </Row>
              </div>
            </ResultsContainer>
          </Row>
        }
      </Container>
    );
  }
}

export default Results;
