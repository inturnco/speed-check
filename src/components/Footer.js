import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';
import Row from './Row';

const Footer = styled.div`
  background: #111c25;
  display: none;
  color: rgba(183, 222, 255, 0.41);
  padding: 10px;
  font-size: 10px;

  ${({ isRevealed }) => isRevealed && css`
    display: block;
  `}

  a {
    color: rgba(183, 222, 255, 0.8);
  }
`;

const propTypes = {
  isRevealed: PropTypes.bool.isRequired,
};

const $Footer = ({ isRevealed }) => (
  <Footer isRevealed={isRevealed}>
    <Row>
      <p style={{ width: '100%', textAlign: 'center' }}>
        <sup>*</sup> Network speed based on information from
        {' '}<a rel="noopener noreferrer" target="_blank" href="https://www.speedtest.net/global-index">speedtest.net</a>
        {' '}│ Browser benchmark is a lightly modified version
        {' '}<a rel="noopener noreferrer" target="_blank" href="https://krakenbenchmark.mozilla.org/">Kraken</a> by Mozilla
      </p>
    </Row>
  </Footer>
);

$Footer.propTypes = propTypes;

export default $Footer;
