import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';
import { colors } from '../../constants';

const Square = styled.div`
  height: 15px;
  width: 15px;
  background: #c8cad6;
  transition: background 0.2s;

  ${({ status }) => status && status === 'ACTIVE' && css`
    background: ${colors.negative};
  `}

  ${({ status }) => status && status === 'COMPLETED' && css`
    background: ${colors.positive};
  `}
`;

const $Square = ({ status }) => <Square status={status} />;

$Square.propTypes = {
  status: PropTypes.oneOf(['INACTIVE', 'ACTIVE', 'COMPLETED']).isRequired,
};

export default $Square;
